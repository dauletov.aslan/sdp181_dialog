package kz.astana.dialogexample;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DialogInterface.OnClickListener {

    private SimpleDateFormat dateFormat, timeFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        Button alertDialog = findViewById(R.id.alertDialogButton);
        alertDialog.setOnClickListener(this);
        Button alertDialogWithSingle = findViewById(R.id.alertDialogWithSingleButton);
        alertDialogWithSingle.setOnClickListener(this);
        Button alertDialogWithMultiple = findViewById(R.id.alertDialogWithMultipleButton);
        alertDialogWithMultiple.setOnClickListener(this);
        Button datePickerDialog = findViewById(R.id.datePickerDialogButton);
        datePickerDialog.setOnClickListener(this);
        Button timePickerDialog = findViewById(R.id.timePickerDialogButton);
        timePickerDialog.setOnClickListener(this);
        Button progressDialog = findViewById(R.id.progressDialogButton);
        progressDialog.setOnClickListener(this);
        Button horizontalProgressDialog = findViewById(R.id.horizontalProgressDialogButton);
        horizontalProgressDialog.setOnClickListener(this);
        Button customDialog = findViewById(R.id.customDialogButton);
        customDialog.setOnClickListener(this);
        Button firstFragmentDialog = findViewById(R.id.firstFragmentDialogButton);
        firstFragmentDialog.setOnClickListener(this);
        Button secondFragmentDialog = findViewById(R.id.secondFragmentDialogButton);
        secondFragmentDialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alertDialogButton:
                showAlertDialog();
                break;
            case R.id.alertDialogWithSingleButton:
                showSingleChoiceDialog();
                break;
            case R.id.alertDialogWithMultipleButton:
                showMultiChoiceDialog();
                break;
            case R.id.datePickerDialogButton:
                showDatePickerDialog();
                break;
            case R.id.timePickerDialogButton:
                showTimePickerDialog();
                break;
            case R.id.progressDialogButton:
                showProgressDialog();
                break;
            case R.id.horizontalProgressDialogButton:
                showHorizontalProgressDialog();
                break;
            case R.id.customDialogButton:
                showCustomDialog();
                break;
            case R.id.firstFragmentDialogButton:
                FirstDialogFragment dialogFragment = new FirstDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "First dialog");
                break;
            case R.id.secondFragmentDialogButton:
                SecondDialogFragment dialog = new SecondDialogFragment();
                dialog.show(getSupportFragmentManager(), "Second dialog");
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            dialog.dismiss();
        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
            dialog.dismiss();
        } else if (which == DialogInterface.BUTTON_NEUTRAL) {
            dialog.dismiss();
        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Warning");
        builder.setMessage("It's simple alert dialog");
        builder.setIcon(R.drawable.ic_empathy);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", this);
        builder.setNegativeButton("Cancel", this);
        builder.setNeutralButton("Maybe", this);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showSingleChoiceDialog() {
        String[] s = {"One", "Two", "Three"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Single choice");
        builder.setIcon(R.drawable.ic_empathy);
        builder.setItems(s, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, s[which], Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showMultiChoiceDialog() {
        String[] s = {"One", "Two", "Three"};
        boolean[] b = {false, true, false};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Multi choice");
        builder.setMultiChoiceItems(s, b, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                b[which] = isChecked;
            }
        });
        builder.setPositiveButton("Show me", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < s.length; i++) {
                    if (b[i]) {
                        sb.append(s[i]).append(" is checked\n");
                    } else {
                        sb.append(s[i]).append(" is not checked\n");
                    }
                }
                Toast.makeText(MainActivity.this, sb.toString(), Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDatePickerDialog() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            DatePickerDialog dialog = new DatePickerDialog(MainActivity.this);
            dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);
                    Toast.makeText(MainActivity.this, dateFormat.format(calendar.getTime()), Toast.LENGTH_LONG).show();
                }
            });
            Calendar calendar = Calendar.getInstance();
            dialog.updateDate(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );
            calendar.set(2020, 8, 10);
            dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            calendar.set(2020, 11, 10);
            dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            dialog.show();
        }
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        TimePickerDialog dialog = new TimePickerDialog(
                MainActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar c = Calendar.getInstance();
                        c.set(0, 0, 0, hourOfDay, minute);
                        Toast.makeText(MainActivity.this, timeFormat.format(c.getTime()), Toast.LENGTH_LONG).show();
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );
        dialog.show();
    }

    private void showProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Download");
        dialog.setMessage("Downloading...");
        dialog.show();
    }

    private void showHorizontalProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setTitle("Download");
        dialog.setMessage("Downloading...");
        dialog.setMax(500);
        dialog.show();
        dialog.setProgress(100);
    }

    private void showCustomDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.layout_dialog, null);
        builder.setView(view);
        builder.setTitle("Custom dialog");
        builder.setPositiveButton("Ok", this);

        ImageView imageView = view.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.ic_empathy);
        TextView textView = view.findViewById(R.id.textView);
        textView.setText("Bye bye");

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}