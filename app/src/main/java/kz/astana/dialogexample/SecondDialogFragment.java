package kz.astana.dialogexample;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class SecondDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Title");
        builder.setMessage("My message");
        builder.setPositiveButton("Yes", this);
        builder.setNeutralButton("Maybe", this);
        builder.setNegativeButton("No", this);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case Dialog.BUTTON_NEGATIVE:
            case Dialog.BUTTON_NEUTRAL:
            case Dialog.BUTTON_POSITIVE:
                dismiss();
        }
    }
}
